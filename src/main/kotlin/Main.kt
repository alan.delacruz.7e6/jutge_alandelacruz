import java.util.*
val scanner = Scanner(System.`in`)
class Problema(
    val enunciat: String,
    val jocpublic: String,
    val jocprivat: Pair<String, String>,
    var resolt: Boolean = false
) {
    var intents = 0
    var llistaintents = mutableListOf<String>()

    /**Mostra l'enunciat i el joc públic i després l'usuari ha de decidir si vol resoldre el problema.*/
    fun showProblem(): Boolean {
        println(enunciat)
        println(jocpublic)
        println(jocprivat.first)
        println("Vols resoldre aquest problema? SI | NO")
        val resposta = readLine()
        if (resposta?.toUpperCase() == "SI") {
            showJocProva()
        }
        return true
    }

    /**Es mostra el joc public i privat i demana una solució.*/
    fun showJocProva() {
        println("Juego de prueba privado: ")
        println(jocprivat.first)
        val inputsolution = scanner.next()
        checkAnswer(inputsolution.toString())
    }

    /**Revisa la solució del usuari i si és correcte l'indicador es canvia a true.*/
    fun checkAnswer(solution: String): Boolean {
        intents++
        llistaintents.add(solution)
        return if (jocprivat.second == solution) {
            println("La resposta és correcta.")
            println("Número dels intents : $intents \nRegistre dels teus intents : $llistaintents")
            resolt = true
            true
        } else {
            println("La resposta és incorrecta")
            showProblem()
            resolt = false
            false
        }
    }
}

class Problemas {

    /**S'itera en la llista d'enunciats i espera a que l'usuari indiqui una resposta. El codi entra en un bucle mentre que el poblema no es resol*/
    fun ResoltEx() {

        for (exercici in enunciats) {
            if (!exercici.resolt && !exercici.showProblem()) {
                while (!exercici.resolt) {
                    val usersolution = scanner.next()
                    exercici.resolt = exercici.checkAnswer(usersolution)
                }
            }
        }

    }

    val enunciats =
        listOf(
            Problema("Dobla l'enter: ", "2 = 4", Pair("7 = ?", "14")),
            Problema("Suma de dos nombres enters: ", "4 + 5 = 9", Pair("6 + 5 = ?", "11")),
            Problema("Calcula l'àrea", "3  5 = 15", Pair("12  12 = ?", "144")),
            Problema("Afegeix un segon", "59 = 0", Pair("45 = ?", "46")),
            Problema("Transforma l'enter", "24 = 24.0", Pair("45 = ?", "45.0"))
        )
}


fun main(args: Array<String>) {

    Problemas().ResoltEx()


}